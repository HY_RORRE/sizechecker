#include <iostream>
using namespace std;

int main(void){
	cout << "short\t\t: " << sizeof(short) << endl;
	cout << "int\t\t\t: " << sizeof(int) << endl;
	cout << "long\t\t: " << sizeof(long) << endl;
	cout << "long long\t: " << sizeof(long long) << endl << endl;
	cout << "size_t\t\t: " << sizeof(size_t) << endl << endl;
	cout << "ptr\t\t\t: " << sizeof(int*) << endl;
	cout << "ptrdiff_t\t: " << sizeof(ptrdiff_t) << endl << endl;
}
